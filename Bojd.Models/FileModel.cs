﻿namespace Bojd.Models
{
    public class FileModel
    {
        public FileModel()
        {
        }

        public int BucketId { get; set; }
        public string Content { get; set; }
        public float Extension { get; set; }
        public string Name { get; set; }
        public int Size { get; set; }   // in bytes
        public int UserId { get; set; }
    }
}