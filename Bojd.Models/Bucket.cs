﻿namespace Bojd.Models
{
    public class Bucket
    {
        public Bucket(int userId, int bucketSize, string name, int bucketId, bool publicFlag, int groupId = 0)
        {
            UserId = userId;
            Size = bucketSize;
            Name = name;
            BucketId = bucketId;
            PublicFlag = publicFlag;
            GroupId = groupId;
        }

        public Bucket()
        {
        }

        public int BucketId { get; set; }
        public int GroupId { get; set; }
        public string Name { get; set; }
        public bool PublicFlag { get; set; }
        public int Size { get; set; }
        public float SpaceLeft { get; set; }
        public int UserId { get; set; }
    }
}