﻿namespace Bojd.Models
{
    public class Lambda
    {
		public string LambdaName { get; set; }
		public int Id { get; set; }
		public int UserID { get; set; }
		public string Technology { get; set; }
		public string Type { get; set; }
		public string Trigger { get; set; }
		public string ImageId { get; set; }
		public int Executed { get; set; }
		public double Average { get; set; }
		public double Total { get; set; }

		public Lambda()
		{
		}

		public Lambda(string lambdaName, int userID, string technology)
		{
			LambdaName = lambdaName;
			UserID = userID;
			Technology = technology;
		}

	}
}
