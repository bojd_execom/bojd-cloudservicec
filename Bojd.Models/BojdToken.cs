﻿namespace Bojd.Models
{
    public class BojdToken
    {
        public long ExpiresIn { get; set; }
        public string Value { get; set; }
    }
}