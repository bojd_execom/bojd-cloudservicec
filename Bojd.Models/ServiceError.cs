﻿namespace Bojd.Models
{
    public class ServiceError
    {
        public ServiceError(int code, string text)
        {
            Code = code;
            Text = text;
        }

        public int Code { get; set; }
        public string Text { get; set; }
    }
}