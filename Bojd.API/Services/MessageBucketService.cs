﻿using Bojd.API.Utils;
using Bojd.Common;
using Bojd.Messages;
using Bojd.Models;
using NetMQ;
using System.Collections.Generic;

namespace Bojd.API.Services
{
    public class MessageBucketService : IBucketService
    {
        public FileModel FileModel { get; private set; }

        public Either<ServiceError, bool> Add(Bucket bucket)
        {
            NetMQMessage message = SocketExtensions.SendRequestAndGetResponse(new CreateBucket(bucket.UserId, bucket.Size, bucket.Name, bucket.BucketId, bucket.PublicFlag).ToNetMQMessage());

            if (message == null)
            {
                return new ServiceError(500, "Request to add bucket timed out, no response from bucket management service.");
            }

            var topic = message[0].ConvertToString();

            if (topic.StartsWith("Publish.Error"))
            {
                return ErrorConvertor.ConvertMessageErrorToServiceError(message);
            }

            return true;
        }

        public Either<ServiceError, bool> Delete(int bucketId, int userId)
        {
            NetMQMessage message = SocketExtensions.SendRequestAndGetResponse(new DeleteBucket(userId, bucketId).ToNetMQMessage());

            if (message == null)
            {
                return new ServiceError(500, "Request to delete bucket timed out, no response from bucket management service.");
            }

            var topic = message[0].ConvertToString();

            if (topic.StartsWith("Publish.Error"))
            {
                return ErrorConvertor.ConvertMessageErrorToServiceError(message);
            }

            return true;
        }

        public Either<ServiceError, bool> Edit(Bucket bucket)
        {
            NetMQMessage message = SocketExtensions.SendRequestAndGetResponse(new EditBucket(bucket.UserId, bucket.Size, bucket.Name, bucket.BucketId, bucket.PublicFlag).ToNetMQMessage());

            if (message == null)
            {
                return new ServiceError(500, "Request to eidt bucket timed out, no response from bucket management service.");
            }

            var topic = message[0].ConvertToString();

            if (topic.StartsWith("Publish.Error"))
            {
                return ErrorConvertor.ConvertMessageErrorToServiceError(message);
            }

            return true;
        }

        public Either<ServiceError, Bucket> Get(int userId, int bucketId)
        {
            NetMQMessage message = SocketExtensions.SendRequestAndGetResponse(new GetBucket(userId, bucketId).ToNetMQMessage());

            if (message == null)
            {
                return new ServiceError(500, "Request to get bucket timed out, no response from bucket management service.");
            }

            var topic = message[0].ConvertToString();

            if (topic.StartsWith("Publish.Error"))
            {
                return ErrorConvertor.ConvertMessageErrorToServiceError(message);
            }

            BucketContent bucketMessage = new BucketContent(message);

            Bucket bucket = new Bucket
            {
                BucketId = bucketMessage.Content.BucketId,
                Name = bucketMessage.Content.Name,
                Size = bucketMessage.Content.Size,
                PublicFlag = bucketMessage.Content.Public
            };
            return bucket;
        }

        public Either<ServiceError, List<Bucket>> GetAll(int userId)
        {
            NetMQMessage message = SocketExtensions.SendRequestAndGetResponse(new GetAllBuckets(userId).ToNetMQMessage());

            if (message == null)
            {
                return new ServiceError(500, "Request to get all buckets timed out, no response from bucket management service.");
            }

            var topic = message[0].ConvertToString();

            if (topic.StartsWith("Publish.Error"))
            {
                return ErrorConvertor.ConvertMessageErrorToServiceError(message);
            }

            BucketList bucketList = new BucketList(message);
            return ConvertMessageBucketListToBucketList(bucketList);
        }

        public Either<ServiceError, FileModel> GetFile(int bucketId, string fileName)
        {
            NetMQMessage message = SocketExtensions.SendRequestAndGetResponse(new GetFile(bucketId, fileName).ToNetMQMessage());

            if (message == null)
            {
                return new ServiceError(500, "Request to get bucket file timed out, no response from bucket management service.");
            }

            var topic = message[0].ConvertToString();

            if (topic.StartsWith("Publish.Error"))
            {
                return ErrorConvertor.ConvertMessageErrorToServiceError(message);
            }

            FileContent fileMessage = new FileContent(message);
            FileModel file = new FileModel
            {
                BucketId = fileMessage.Content.BucketId,
                Name = fileMessage.Content.FileName,
                Content = fileMessage.Content.FileContent.ToBase64()
            };
            return file;
        }

        public Either<ServiceError, List<FileModel>> GetFiles(int bucketId)
        {
            NetMQMessage message = SocketExtensions.SendRequestAndGetResponse(new GetFiles(bucketId).ToNetMQMessage());

            if (message == null)
            {
                return new ServiceError(500, "Request to get bucket files timed out, no response from bucket management service.");
            }

            var topic = message[0].ConvertToString();

            if (topic.StartsWith("Publish.Error"))
            {
                return ErrorConvertor.ConvertMessageErrorToServiceError(message);
            }

            FileList fileList = new FileList(message);
            return ConvertMessageFileListToFileList(fileList);
        }

        public Either<ServiceError, bool> UploadFile(byte[] fileInBytes, int bucketId, string fileName, string extension, int size = 0)
        {
            NetMQMessage message = SocketExtensions.SendRequestAndGetResponse(new UploadFile(fileInBytes, bucketId, fileName, extension, size).ToNetMQMessage());

            if (message == null)
            {
                return new ServiceError(500, "Request to upload file to bucket timed out, no response from bucket management service.");
            }

            var topic = message[0].ConvertToString();

            if (topic.StartsWith("Publish.Error"))
            {
                return ErrorConvertor.ConvertMessageErrorToServiceError(message);
            }

            return true;
        }

        private List<Bucket> ConvertMessageBucketListToBucketList(BucketList messageBuckets)
        {
            List<Bucket> retList = new List<Bucket>();
            foreach (var messageBucket in messageBuckets.Content.Buckets)
            {
                Bucket bucket = new Bucket
                {
                    BucketId = messageBucket.BucketId,
                    Name = messageBucket.Name,
                    UserId = messageBucket.UserId,
                    Size = messageBucket.Size,
                    SpaceLeft = messageBucket.SpaceLeft,
                    PublicFlag = messageBucket.Public
                };
                retList.Add(bucket);
            }
            return retList;
        }

        private List<FileModel> ConvertMessageFileListToFileList(FileList messageFiles)
        {
            List<FileModel> retList = new List<FileModel>();
            foreach (var messageFile in messageFiles.Content.Files)
            {
                FileModel file = new FileModel
                {
                    BucketId = messageFile.BucketId,
                    Name = messageFile.FileName,
                    Size = messageFile.Size
                };
                retList.Add(file);
            }
            return retList;
        }
    }
}