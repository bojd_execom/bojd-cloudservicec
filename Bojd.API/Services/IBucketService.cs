﻿using Bojd.Common;
using Bojd.Models;
using System.Collections.Generic;

namespace Bojd.API.Services
{
    public interface IBucketService
    {
        Either<ServiceError, bool> Add(Bucket bucket);

        Either<ServiceError, bool> Delete(int bucketId, int userId);

        Either<ServiceError, bool> Edit(Bucket bucket);

        Either<ServiceError, Bucket> Get(int userId, int bucketId);

        Either<ServiceError, List<Bucket>> GetAll(int userId);

        Either<ServiceError, FileModel> GetFile(int bucketId, string fileName);

        Either<ServiceError, List<FileModel>> GetFiles(int bucketId);

        Either<ServiceError, bool> UploadFile(byte[] fileInBytes, int bucketId, string fileName, string extension, int size = 0);
    }
}