﻿using Bojd.API.Utils;
using Bojd.Common;
using Bojd.Messages;
using Bojd.Models;
using NetMQ;
using System.Collections.Generic;


namespace Bojd.API.Services
{
    public class MessageLambdaService : ILambdaService
    {
        public Either<ServiceError, string> Delete(int id, int userId)
        {
            NetMQMessage message = SocketExtensions.SendRequestAndGetResponse(new DeleteLambda(id, userId).ToNetMQMessage());

            if (message == null)
            {
                return new ServiceError(500, "Request to delete lambda timed out, no response from lambda management service.");
            }

            var topic = message[0].ConvertToString();

			if (topic.StartsWith("Publish.Error"))
            {
				return ErrorConvertor.ConvertMessageErrorToServiceError(message);
			}

			var content = LambdaFinishedConvertor.ConvertLambdaFinishedMessageToText(message);

			return content;
        }

        public Either<ServiceError, string> Edit(byte[] fileBytes, int id, int userId)
        {
            NetMQMessage message = SocketExtensions.SendRequestAndGetResponse(new EditLambda(fileBytes, id, userId).ToNetMQMessage());

            if (message == null)
            {
                return new ServiceError(500, "Request to edit lambda timed out, no response from lambda management service.");
            }

            var topic = message[0].ConvertToString();

            if (topic.StartsWith("Publish.Error"))
            {
				return ErrorConvertor.ConvertMessageErrorToServiceError(message);
			}

			var content = LambdaFinishedConvertor.ConvertLambdaFinishedMessageToText(message);

			return content;
        }

        public Either<ServiceError, List<Lambda>> GetAll(int userId)
        {
            NetMQMessage message = SocketExtensions.SendRequestAndGetResponse(new GetAllLambdas(userId).ToNetMQMessage());

            if (message == null)
            {
                return new ServiceError(500, "Request to get all lambdas timed out, no response from lambda management service.");
            }

            LambdaList lambdas = new LambdaList(message);

            return ConvertMessageLambdaListToLambdaList(lambdas);
        }

        public Either<ServiceError, Lambda> GetSingleLambda(int lambdaId)
        {
            NetMQMessage message = SocketExtensions.SendRequestAndGetResponse(new GetSingleLambda(lambdaId).ToNetMQMessage());

            if (message == null)
            {
                return new ServiceError(500, "Request to get lambda timed out, no response from lambda management service.");
            }

            SingleLambdaInfo retVal = new SingleLambdaInfo(message);

            return ConvertMessageSingleLambdaInfoToLambda(retVal);
        }

        public Either<ServiceError, string> RunLambda(string guid)
        {
            NetMQMessage message = SocketExtensions.SendRequestAndGetResponse(new RunLambda(guid).ToNetMQMessage());

            if (message == null)
            {
                return new ServiceError(500, "Request to run lambda timed out, no response from lambda management service.");
            }

            var topic = message[0].ConvertToString();

            if (topic.StartsWith("Publish.Error"))
            {
				return ErrorConvertor.ConvertMessageErrorToServiceError(message);
			}

			var content = LambdaFinishedConvertor.ConvertLambdaFinishedMessageToText(message);

			return content;
        }

		public Either<ServiceError, string> RunPostLambda(string json, string guid)
		{
			NetMQMessage message = SocketExtensions.SendRequestAndGetResponse(new RunPostLambda(json, guid).ToNetMQMessage());

			if (message == null)
			{
				return new ServiceError(500, "Request to run lambda timed out, no response from lambda management service.");
			}

			var topic = message[0].ConvertToString();

			if (topic.StartsWith("Publish.Error"))
			{
				return ErrorConvertor.ConvertMessageErrorToServiceError(message);
			}

			var content = LambdaFinishedConvertor.ConvertLambdaFinishedMessageToText(message);

			return content;
		}

        public Either<ServiceError, string> UploadLambda(byte[] fileInBytes, string lambdaName, string technology, string type, string trigger, int userId)
        {
            NetMQMessage message = SocketExtensions.SendRequestAndGetResponse(new UploadLambda(fileInBytes, lambdaName, technology, type, trigger, userId).ToNetMQMessage());

            if (message == null)
            {
                return new ServiceError(500, "Request to upload lambda timed out, no response from lambda management service.");
            }

            var topic = message[0].ConvertToString();
			
			if (topic.StartsWith("Publish.Error"))
            {
				return ErrorConvertor.ConvertMessageErrorToServiceError(message);
			}

			var content = LambdaFinishedConvertor.ConvertLambdaFinishedMessageToText(message);

			return content;
        }

        private List<Lambda> ConvertMessageLambdaListToLambdaList(LambdaList messageLambdaList)
        {
            List<Lambda> retList = new List<Lambda>();

            foreach (var messageLambda in messageLambdaList.Content.Lambdas)
            {
                Lambda lambda = new Lambda()
                {
                    Id = messageLambda.Id,
                    LambdaName = messageLambda.Name,
                    Technology = messageLambda.Technology,
                    UserID = messageLambda.UserId
                };
                retList.Add(lambda);
            }

            return retList;
        }

        private Lambda ConvertMessageSingleLambdaInfoToLambda(SingleLambdaInfo message)
        {
            Lambda retVal = new Lambda()
            {
                Id = message.Content.Id,
                LambdaName = message.Content.Name,
                Technology = message.Content.Technology,
                Type = message.Content.Type,
                Trigger = message.Content.Trigger,
                ImageId = message.Content.ImageId,
                Average = message.Content.Average,
                Executed = message.Content.Executed,
                Total = message.Content.Total
            };

            return retVal;
        }
    }
}