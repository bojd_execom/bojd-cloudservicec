﻿using Bojd.Common;
using Bojd.Models;

namespace Bojd.API
{
    public interface IUserService
    {
        Either<ServiceError, bool> Add(User user);

        Either<ServiceError, BojdToken> LogOn(User user);
    }
}