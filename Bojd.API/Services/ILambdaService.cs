﻿using Bojd.Common;
using Bojd.Models;
using System.Collections.Generic;

namespace Bojd.API.Services
{
    public interface ILambdaService
    {
        Either<ServiceError, string> Delete(int id, int userId);

        Either<ServiceError, string> Edit(byte[] fileBytes, int id, int userId);

        Either<ServiceError, List<Lambda>> GetAll(int userId);

        Either<ServiceError, Lambda> GetSingleLambda(int lambdaId);

        Either<ServiceError, string> RunLambda(string id);

        Either<ServiceError, string> UploadLambda(byte[] fileInBytes, string lambdaName, string technology, string type, string trigger, int userId); // technology can also be of enum type

		Either<ServiceError, string> RunPostLambda(string jsonContent, string guid);
    }
}