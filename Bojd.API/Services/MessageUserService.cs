﻿using Bojd.API.Utils;
using Bojd.Common;
using Bojd.Messages;
using Bojd.Models;
using NetMQ;

namespace Bojd.API.Services
{
    public class MessageUserService : IUserService
    {
        public Either<ServiceError, bool> Add(User user)
        {
            NetMQMessage message = SocketExtensions.SendRequestAndGetResponse(new CreateUser(user.Id, user.UserName, user.Password).ToNetMQMessage());

            if (message == null)
            {
                return new ServiceError(500, "Request timed out, no response from user management service.");
            }

            var topic = message[0].ConvertToString();

            if (topic.StartsWith("Publish.Error"))
            {
                return ErrorConvertor.ConvertMessageErrorToServiceError(message);
            }

            return true;
        }

        public Either<ServiceError, BojdToken> LogOn(User user)
        {
            NetMQMessage message = SocketExtensions.SendRequestAndGetResponse(new LogOnUser(user.Id, user.UserName, user.Password).ToNetMQMessage());

            if (message == null)
            {
                return new ServiceError(500, "Request timed out, no response from user management service.");
            }

            var topic = message[0].ConvertToString();

            if (topic.StartsWith("Publish.Error"))
            {
                return ErrorConvertor.ConvertMessageErrorToServiceError(message);
            }

            UserLoggedOn userLoggedMessage = new UserLoggedOn(message);

            BojdToken bojdToken = new BojdToken { Value = userLoggedMessage.Content.Value, ExpiresIn = userLoggedMessage.Content.ExpiresIn = 50 };

            return bojdToken;
        }
    }
}