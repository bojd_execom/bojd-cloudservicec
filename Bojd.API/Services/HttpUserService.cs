﻿// DEPRECATED

using Bojd.Common;
using Bojd.Models;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;

namespace Bojd.API.Services
{
    public class HttpUserService : IUserService
    {
        private IHttpClient _client;

        public HttpUserService()
        {
            _client = new Common.HttpClientHandler();
        }

        public HttpUserService(IHttpClient client)
        {
            _client = client;
        }

        public Either<ServiceError, bool> Add(User user)
        {
            var jsonInString = JsonConvert.SerializeObject(user);

            var response = _client.PostAsync("http://localhost:55000/api/auth", new StringContent(jsonInString, Encoding.UTF8, "application/json")).Result;

            return response.StatusCode != System.Net.HttpStatusCode.BadRequest;
        }

        public Either<ServiceError, BojdToken> LogOn(User user)
        {
            var jsonInString = JsonConvert.SerializeObject(user);

            var response = _client.PostAsync("http://localhost:55000/api/auth/authenticate", new StringContent(jsonInString, Encoding.UTF8, "application/json")).Result;

            if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                return null;
            }

            var responseString = response.Content.ReadAsStringAsync();

            BojdToken oMycustomclassname = JsonConvert.DeserializeObject<BojdToken>(responseString.Result);

            return oMycustomclassname;
        }
    }
}