﻿using NetMQ;
using NetMQ.Sockets;
using System;

namespace Bojd.API.Utils
{
    public static class SocketExtensions
    {
        public static NetMQMessage SendRequestAndGetResponse(NetMQMessage message)
        {
            RequestSocket requestSocket = new RequestSocket();
            requestSocket.Connect(Configuration.AgregatorRouterAddress);
            requestSocket.SendMultipartMessage(message);
            NetMQMessage retMsg = new NetMQMessage();
            requestSocket.TryReceiveMultipartMessage(new TimeSpan(0, 0, 10), ref retMsg);
            requestSocket.Close();
            requestSocket.Dispose();
            if (retMsg.FrameCount == 0)
            {
                return null;
            }
            return retMsg;
        }
    }
}