﻿using Bojd.Messages;
using NetMQ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bojd.API.Utils
{
    public class LambdaFinishedConvertor
    {
		public static string ConvertLambdaFinishedMessageToText(NetMQMessage message)
		{
			LambdaFinished lambdaFinished = new LambdaFinished(message);
			return lambdaFinished.Content.Message;
		}
	}
}
