﻿using Bojd.Messages;
using Bojd.Models;
using NetMQ;

namespace Bojd.API.Utils
{
    public static class ErrorConvertor
    {
        public static ServiceError ConvertMessageErrorToServiceError(NetMQMessage messageError)
        {
            Error error = new Error(messageError);
            return new ServiceError(error.Content.Code, error.Content.Text);
        }
    }
}