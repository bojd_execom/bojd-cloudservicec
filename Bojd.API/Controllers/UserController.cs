﻿using Bojd.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Bojd.API.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        // DELETE api/user/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            return Ok();
        }

        // GET api/user
        [Authorize]
        [HttpGet]
        public IActionResult Get()
        {
            return Ok("dobro je");
        }

        // GET api/user/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        [HttpPost("logon")]
        public IActionResult LogOn([FromBody]User user)
        {
            var result = _userService.LogOn(user);
            return result.Match<ActionResult>(
                error => StatusCode(error.Code, error.Text),
                token => Ok(token)

            );
        }

        // POST api/user
        [HttpPost]
        public IActionResult Post([FromBody]User user)
        {
            var result = _userService.Add(user);
            return result.Match<ActionResult>(
                error => StatusCode(error.Code, error.Text),
                token => Ok("Successfully created user!")

            );
        }

        // PUT api/user/5
        [HttpPut("{id}")]
        public IActionResult Put([FromBody]User user)
        {
            return Ok();
        }
    }
}