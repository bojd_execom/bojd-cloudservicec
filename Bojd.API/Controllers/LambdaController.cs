﻿using Bojd.API.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;

namespace Bojd.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LambdaController : ControllerBase
    {
        private ILambdaService _lambdaService;

        public LambdaController(ILambdaService lambndaService)
        {
            _lambdaService = lambndaService;
        }

        [Authorize]
        [HttpPost]
        public IActionResult Create()
        {
            string name = Request.Form["name"];
            string language = Request.Form["language"];
            string type = Request.Form["type"];
            string trigger = Request.Form["trigger"];
            int userId = ExtractUserId((ClaimsIdentity)User.Identity);

            if (Request.Form.Files.Count > 0 && name.Length > 0 && language.Length > 0)
            {
                var file = Request.Form.Files[0];

                using (var ms = new MemoryStream())
                {
                    file.CopyTo(ms);
                    var fileBytes = ms.ToArray();

                    var result = _lambdaService.UploadLambda(fileBytes, name, language, type, trigger, userId);

                    return result.Match<ActionResult>(
                        error => StatusCode(error.Code, error.Text),
                        guid => Ok(guid)
                    );
                }
            }

            return BadRequest("You must enter valid data.");
        }

        [Authorize]
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            int userId = ExtractUserId((ClaimsIdentity)User.Identity);
            var result = _lambdaService.Delete(id, userId);
            return result.Match<ActionResult>(
                error => StatusCode(error.Code, error.Text),
                success => Ok()
            );
        }

        [HttpGet("run")]
        public IActionResult Get(string guid)
        {
            var result = _lambdaService.RunLambda(guid); //protobuf's convert to string puts % infront
            return result.Match<ActionResult>(
                error => StatusCode(error.Code, error.Text),
                resultString => Ok(resultString)
            );
        }

		[HttpPost("{guid}/run")]
		public IActionResult Post([FromBody]JToken jsonBody, string guid)
		{
			string jsonBodyString = jsonBody.ToString();

			var result = _lambdaService.RunPostLambda(jsonBodyString, guid);

			return result.Match<ActionResult>(
				error => StatusCode(error.Code, error.Text),
				resultString => Ok(resultString)
			);

		}

		[Authorize]
        [HttpGet]
        public IActionResult Get()
        {
            var userId = ExtractUserId((ClaimsIdentity)User.Identity);
            var result = _lambdaService.GetAll(userId);
            return result.Match<ActionResult>(
                error => StatusCode(error.Code, error.Text),
                lambdas => Ok(lambdas)
            );
        }

        [Authorize]
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var result = _lambdaService.GetSingleLambda(id);
            return result.Match<ActionResult>(
                error => StatusCode(error.Code, error.Text),
                lambda => Ok(lambda)
            );
        }

        [Authorize]
        [HttpPut("{id}")]
        public IActionResult Put(int id)
        {
            int userId = ExtractUserId((ClaimsIdentity)User.Identity);

            if (Request.Form.Files.Count > 0)
            {
                var file = Request.Form.Files[0];

                using (var ms = new MemoryStream())
                {
                    file.CopyTo(ms);
                    var fileBytes = ms.ToArray();

                    var result = _lambdaService.Edit(fileBytes, id, userId);
                    return result.Match<ActionResult>(
                        error => StatusCode(error.Code, error.Text),
                        retVal => Ok(retVal)
                    );
                }
            }

            return Ok();
        }

        private int ExtractUserId(ClaimsIdentity identity)
        {
            IEnumerable<Claim> claims = identity.Claims;
            var userId = claims.FirstOrDefault(claim => claim.Type == ClaimTypes.Sid).Value;
            return int.Parse(userId);
        }
    }
}