﻿using Bojd.API.Services;
using Bojd.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;

namespace Bojd.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BucketController : ControllerBase
    {
        private IBucketService _bucketService;

        public BucketController(IBucketService bucketService)
        {
            _bucketService = bucketService;
        }

        [Authorize]
        [HttpDelete("{bucketId}")]
        public IActionResult Delete(int bucketId)
        {
            var userId = ExtractUserId((ClaimsIdentity)User.Identity);
            var result = _bucketService.Delete(bucketId, userId);
            return result.Match<ActionResult>(
                error => StatusCode(error.Code, error.Text),
                success => Ok("Successfully deleted bucket!")
            );
        }

        [Authorize]
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            int userId = ExtractUserId((ClaimsIdentity)User.Identity);
            var result = _bucketService.Get(userId, id);
            return result.Match<ActionResult>(
                error => StatusCode(error.Code, error.Text),
                bucket => Ok(bucket)
            );
        }

        [Authorize]
        [HttpGet]
        public IActionResult GetAll()
        {
            int userId = ExtractUserId((ClaimsIdentity)User.Identity);
            var result = _bucketService.GetAll(userId);
            return result.Match<ActionResult>(
                error => StatusCode(error.Code, error.Text),
                buckets => Ok(buckets)
            );
        }

        [Authorize]
        [HttpGet("{bucketId}/file/{fileName}")]
        public IActionResult GetFile(int bucketId, string fileName)
        {
            var result = _bucketService.GetFile(bucketId, fileName);
            return result.Match<ActionResult>(
                error => StatusCode(error.Code, error.Text),
                file => Ok(file)
            );
        }

        [Authorize]
        [HttpGet("{bucketId}/files")]
        public IActionResult GetFiles(int bucketId)
        {
            var result = _bucketService.GetFiles(bucketId);
            return result.Match<ActionResult>(
                error => StatusCode(error.Code, error.Text),
                files => Ok(files)
            );
        }

        [Authorize]
        [HttpPost]
        public IActionResult Post([FromBody]Bucket bucket)
        {
            var userId = ExtractUserId((ClaimsIdentity)User.Identity);
            bucket.UserId = userId;
            var result = _bucketService.Add(bucket);
            return result.Match<ActionResult>(
                error => StatusCode(error.Code, error.Text),
                success => Ok("Successfully created bucket!")
            );
        }

        [Authorize]
        [HttpPut]
        public IActionResult Put([FromBody]Bucket bucket)
        {
            var userId = ExtractUserId((ClaimsIdentity)User.Identity);
            bucket.UserId = userId;
            var result = _bucketService.Edit(bucket);
            return result.Match<ActionResult>(
                error => StatusCode(error.Code, error.Text),
                success => Ok("Successfully edited bucket!")
            );
        }

        [Authorize]
        [HttpPost("files")]
        public IActionResult UploadFile()
        {
            int bucketId = int.Parse(Request.Form["bucketId"]);
            if (Request.Form.Files.Count > 0)
            {
                var file = Request.Form.Files[0];
                string name;
                string extension = string.Empty;
                if (file.FileName.Contains('.'))
                {
                    int i = file.FileName.LastIndexOf('.');
                    extension = file.FileName.Substring(i + 1);
                    name = file.FileName.Substring(0, i);
                }
                else
                {
                    name = file.FileName;
                }

                using (var ms = new MemoryStream())
                {
                    file.CopyTo(ms);

                    var fileBytes = ms.ToArray();

                    var result = _bucketService.UploadFile(fileBytes, bucketId, name, extension);
                    return result.Match<ActionResult>(
                        error => StatusCode(error.Code, error.Text),
                        success => Ok("Successfully uploaded file!")
                    );
                }
            }

            return BadRequest("You must upload file.");
        }

        private int ExtractUserId(ClaimsIdentity identity)
        {
            IEnumerable<Claim> claims = identity.Claims;
            var userId = claims.FirstOrDefault(claim => claim.Type == ClaimTypes.Sid).Value;
            return int.Parse(userId);
        }
    }
}