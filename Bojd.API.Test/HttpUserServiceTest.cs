﻿using System.Net.Http;
using Bojd.API.Services;
using Bojd.Common;
using Bojd.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;

namespace Bojd.API.Test
{
    [TestClass]
    public class HttpUserServiceTest
    {
        [TestMethod]
        public void LogOnUserWithInvalidDataReturnsNullBojdToken()
        {
            User user = new User { UserName = "", Password = "" };

            var mockClient = new Mock<IHttpClient>();
            HttpResponseMessage response = new HttpResponseMessage
            {
                StatusCode = System.Net.HttpStatusCode.BadRequest
            };

            mockClient
                .Setup(x => x.PostAsync(It.IsAny<string>(), It.IsAny<HttpContent>()))
                .ReturnsAsync(response);

            HttpUserService httpUserService = new HttpUserService(mockClient.Object);

            var result = httpUserService.LogOn(user);

            Assert.IsNull(result);
        }

        [TestMethod]
        public void LogOnUserWithValidDataReturnsBojdToken()
        {
            User user = new User { UserName = "ogi", Password = "ogi" };
            string testTokenString = JsonConvert.SerializeObject(new BojdToken() { Value = "testtoken", ExpiresIn = 50 });

            var mockClient = new Mock<IHttpClient>();
            HttpResponseMessage response = new HttpResponseMessage
            {
                Content = new StringContent(testTokenString),
                StatusCode = System.Net.HttpStatusCode.OK
            };

            mockClient
                .Setup(x => x.PostAsync(It.IsAny<string>(), It.IsAny<HttpContent>()))
                .ReturnsAsync(response);

            HttpUserService httpUserService = new HttpUserService(mockClient.Object);

            var result = httpUserService.LogOn(user);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void RegisterUserWithInvalidDataReturnsFalse()
        {
            User user = new User { UserName = "", Password = "" };

            var mockClient = new Mock<IHttpClient>();
            HttpResponseMessage response = new HttpResponseMessage
            {
                StatusCode = System.Net.HttpStatusCode.BadRequest
            };

            mockClient
                .Setup(x => x.PostAsync(It.IsAny<string>(), It.IsAny<HttpContent>()))
                .ReturnsAsync(response);

            HttpUserService httpUserService = new HttpUserService(mockClient.Object);

            var result = httpUserService.Add(user);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void RegisterUserWithValidDataReturnsTrue()
        {
            User user = new User { UserName = "ogi", Password = "ogi" };

            var mockClient = new Mock<IHttpClient>();
            HttpResponseMessage response = new HttpResponseMessage
            {
                StatusCode = System.Net.HttpStatusCode.OK
            };

            mockClient
                .Setup(x => x.PostAsync(It.IsAny<string>(), It.IsAny<HttpContent>()))
                .ReturnsAsync(response);

            HttpUserService httpUserService = new HttpUserService(mockClient.Object);

            var result = httpUserService.Add(user);

            Assert.IsTrue(result);
        }
    }
}