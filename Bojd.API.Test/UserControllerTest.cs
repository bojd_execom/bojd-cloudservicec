using Bojd.API.Controllers;
using Bojd.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Bojd.API.Test
{
    [TestClass]
    public class UserControllerTest
    {
        [TestMethod]
        public void LogOnUserWithInvalidDataReturnsBadRequest()
        {
            //Arrange
            var mockService = new Mock<IUserService>();

            User user = new User { UserName = "", Password = "" };

            mockService
                .Setup(x => x.LogOn(user))
                .Returns((BojdToken)null);

            UserController userController = new UserController(mockService.Object);

            //Act
            var response = userController.LogOn(user);

            //Assert
            Assert.IsInstanceOfType(response, typeof(BadRequestObjectResult));
        }

        [TestMethod]
        public void LogOnUserWithValidDataReturnsOkRequest()
        {
            //Arrange
            var mockService = new Mock<IUserService>();

            User user = new User { UserName = "ogi", Password = "ogi" };

            mockService
                .Setup(x => x.LogOn(user))
                .Returns(new BojdToken() { Value = "testtoken", ExpiresIn = 50 });

            UserController userController = new UserController(mockService.Object);

            //Act
            var response = userController.LogOn(user);

            //Assert
            Assert.IsInstanceOfType(response, typeof(OkObjectResult));
        }

        [TestMethod]
        public void RegisterUserWithInvalidDataReturnsBadRequest()
        {
            var mockService = new Mock<IUserService>();

            User user = new User { UserName = "", Password = "" };

            mockService
                .Setup(x => x.Add(user))
                .Returns(false);

            UserController userController = new UserController(mockService.Object);

            //Act
            var response = userController.Post(user);

            //Assert
            Assert.IsInstanceOfType(response, typeof(BadRequestObjectResult));
        }

        [TestMethod]
        public void RegisterUserWithValidDataReturnsOkRequest()
        {
            //Arrange
            var mockService = new Mock<IUserService>();

            User user = new User { UserName = "ogi", Password = "ogi" };

            mockService
                .Setup(x => x.Add(user))
                .Returns(true);

            UserController userController = new UserController(mockService.Object);

            //Act
            var response = userController.Post(user);

            //Assert
            Assert.IsInstanceOfType(response, typeof(OkObjectResult));
        }
    }
}