﻿using System;

namespace Bojd.Agregator
{
    public class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Starting agregator...");
            CommunicationService communicationService = new CommunicationService();
            communicationService.Start();
        }
    }
}