﻿using Bojd.Common;
using Bojd.Communication.Services;
using NetMQ;
using NetMQ.Sockets;
using System.Threading.Tasks;

namespace Bojd.Agregator
{
    internal class CommunicationService : ServiceBase
    {
        private NetMQPoller _poller;
        private RouterSocket _routerSocket;

        public CommunicationService() : base()
        {
            _routerSocket = new RouterSocket();
            _poller = new NetMQPoller { _routerSocket };

            //TODO: change this to load subscriber topics from configuration file or something
            Subscribe("Publish.UserCreated", HandleBrokerMessage);
            Subscribe("Publish.UserLoggedOn", HandleBrokerMessage);
            Subscribe("Publish.BucketCreated", HandleBrokerMessage);
            Subscribe("Publish.ErrorCreateBucket", HandleBrokerMessage);
            Subscribe("Publish.ErrorCreateUser", HandleBrokerMessage);
            Subscribe("Publish.ErrorLogOnUser", HandleBrokerMessage);
            Subscribe("Publish.ErrorFileUpload", HandleBrokerMessage);
            Subscribe("Publish.ErrorGetFiles", HandleBrokerMessage);
            Subscribe("Publish.FileUploaded", HandleBrokerMessage);
            Subscribe("Publish.GuidGenerated", HandleBrokerMessage);
            Subscribe("Publish.LambdaUploaded", HandleBrokerMessage);
            Subscribe("Publish.LambdaStarted", HandleBrokerMessage);
            Subscribe("Publish.AllBuckets", HandleBrokerMessage);
            Subscribe("Publish.Bucket", HandleBrokerMessage);
            Subscribe("Publish.Files", HandleBrokerMessage);
            Subscribe("Publish.LambdaList", HandleBrokerMessage);
            Subscribe("Publish.LambdaInfo", HandleBrokerMessage);
            Subscribe("Publish.File", HandleBrokerMessage);
            Subscribe("Publish.ErrorGetFile", HandleBrokerMessage);
            Subscribe("Publish.BucketEdited", HandleBrokerMessage);
            Subscribe("Publish.ErrorEditBucket", HandleBrokerMessage);
            Subscribe("Publish.BucketDeleted", HandleBrokerMessage);
            Subscribe("Publish.ErrorDeleteBucket", HandleBrokerMessage);
            Subscribe("Publish.ErrorCreateLambda", HandleBrokerMessage);
            Subscribe("Publish.ErrorRunLambda", HandleBrokerMessage);
            Subscribe("Publish.Error", HandleBrokerMessage);
        }

        public new void Start()
        {
            Task.Run(() => base.Start());   // start service which communicates with broker
            _routerSocket.ReceiveReady += HandleRouterMessageReceived;
            _routerSocket.Bind(Configuration.RouterBindingAddress);
            _poller.Run();
        }

        private void HandleBrokerMessage(NetMQMessage obj)
        {
            NetMQMessage alignedMessage = MessageUtils.ConvertDealerMessageToRouterMessage(obj);
            _routerSocket.SendMultipartMessage(alignedMessage);
        }

        private void HandleRouterMessageReceived(object sender, NetMQSocketEventArgs e)
        {
            NetMQMessage message = null;
            while (e.Socket.TryReceiveMultipartMessage(ref message))
            {
                NetMQMessage alignedMessage = MessageUtils.ConvertRouterMessageToDealerMessage(message);
                Send(alignedMessage);
            }
        }
    }
}