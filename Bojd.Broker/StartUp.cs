﻿using Bojd.Communication.BrokerService;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bojd.BrokerService
{
    public class StartUp
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting Broker...");
            Broker broker = new Broker();
            broker.Start();
        }
    }
}
