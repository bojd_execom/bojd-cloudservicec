﻿using Bojd.Communication.Messages;
using Bojd.Messages.MessageContent;
using NetMQ;

namespace Bojd.Messages
{
    public class UserCreated : MessagePublish<EmptyMessage>
    {
        public UserCreated(NetMQMessage message) : base(message)
        {
        }

        public UserCreated() : base("UserCreated")
        {
        }

        public UserCreated(NetMQFrame idFrame) : base("UserCreated")
        {
            Id = idFrame;
        }
    }
}