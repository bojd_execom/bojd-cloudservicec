﻿using Bojd.Communication.Messages;
using Bojd.Messages.MessageContent;
using NetMQ;

namespace Bojd.Messages
{
    public class BucketContent : MessagePublish<BucketMessage>
    {
        public BucketContent(NetMQMessage message) : base(message)
        {
        }
    }
}