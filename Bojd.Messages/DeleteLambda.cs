﻿using Bojd.Communication.Messages;
using Bojd.Messages.MessageContent;
using NetMQ;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bojd.Messages
{
    public class DeleteLambda : MessagePublish<LambdaMessage>
    {
		public DeleteLambda(NetMQMessage message) : base(message)
		{
		}

		public DeleteLambda(int id, int userId) : base("DeleteLambda")
		{
			Content.Id = id;
			Content.UserId = userId;
		}
    }
}
