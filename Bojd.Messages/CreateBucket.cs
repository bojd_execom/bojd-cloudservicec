﻿using Bojd.Communication.Messages;
using Bojd.Messages.MessageContent;

namespace Bojd.Messages
{
    public class CreateBucket : MessagePublish<BucketMessage>
    {
        public CreateBucket(int userId, int bucketSize, string name, int bucketId, bool publicFlag, int groupId = 0) : base("CreateBucket")
        {
            Content.UserId = userId;
            Content.Size = bucketSize;
            Content.Name = name;
            Content.BucketId = bucketId;
            Content.Public = publicFlag;
            Content.GroupId = groupId;
        }
    }
}