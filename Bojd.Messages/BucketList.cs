﻿using Bojd.Communication.Messages;
using Bojd.Messages.MessageContent;
using NetMQ;

namespace Bojd.Messages
{
    public class BucketList : MessagePublish<BucketsInfoMessage>
    {
        public BucketList(NetMQMessage message) : base(message)
        {
        }
    }
}