﻿using Bojd.Communication.Messages;
using Bojd.Messages.MessageContent;
using NetMQ;

namespace Bojd.Messages
{
    public class GetAllBuckets : MessageGet<UserMessage>
    {
        public GetAllBuckets(NetMQMessage message) : base(message)
        {
        }

        public GetAllBuckets(int userId) : base("AllBuckets")
        {
            Content.Id = userId;
        }
    }
}