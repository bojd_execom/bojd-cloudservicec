﻿using Bojd.Communication.Messages;
using Bojd.Messages.MessageContent;
using NetMQ;

namespace Bojd.Messages
{
	public class FileContent : MessagePublish<FileMessage>
	{
		public FileContent(NetMQMessage message) : base(message)
		{
		}
	}
}