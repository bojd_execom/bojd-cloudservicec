﻿using Bojd.Communication.Messages;
using Bojd.Messages.MessageContent;
using NetMQ;

namespace Bojd.Messages
{
	public class GetFile : MessageGet<FileMessage>
	{
		public GetFile(NetMQMessage message) : base(message)
		{
		}

		public GetFile(int bucketId, string fileName) : base("File")
		{
			Content.FileName = fileName;
			Content.BucketId = bucketId;
		}
	}
}