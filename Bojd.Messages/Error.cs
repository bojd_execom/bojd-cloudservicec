﻿using Bojd.Communication.Messages;
using Bojd.Messages.MessageContent;
using NetMQ;

namespace Bojd.Messages
{
    public class Error : MessagePublish<ErrorMessage>
    {
        public Error(NetMQMessage message) : base(message)
        {
        }

        public Error(NetMQFrame idFrame, int code, string text) : base("Error")
        {
            this.Id = idFrame;
            this.Content.Code = code;
            this.Content.Text = text;
        }
    }
}