﻿using Bojd.Communication.Messages;
using Bojd.Messages.MessageContent;
using NetMQ;

namespace Bojd.Messages
{
    public class CreateUser : MessagePublish<UserMessage>
    {
        public CreateUser(int id, string userName, string password) : base("CreateUser")
        {
            Content.Id = id;
            Content.UserName = userName;
            Content.Password = password;
        }

        public CreateUser(int id, string userName, string password, NetMQFrame idFrame) : base("CreateUser")
        {
            Content.Id = id;
            Content.UserName = userName;
            Content.Password = password;
            Id = idFrame;
        }

        public CreateUser(NetMQMessage message) : base(message)
        {
        }
    }
}