﻿using Bojd.Communication.Messages;
using Bojd.Messages.MessageContent;
using NetMQ;

namespace Bojd.Messages
{
    public class GetBucket : MessageGet<BucketMessage>
    {
        public GetBucket(NetMQMessage message) : base(message)
        {
        }

        public GetBucket(int userId, int bucketId) : base("Bucket")
        {
            Content.UserId = userId;
            Content.BucketId = bucketId;
        }
    }
}