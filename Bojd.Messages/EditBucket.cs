﻿using Bojd.Communication.Messages;
using Bojd.Messages.MessageContent;

namespace Bojd.Messages
{
	public class EditBucket : MessagePublish<BucketMessage>
	{
		public EditBucket(int userId, int bucketSize, string name, int bucketId, bool publicFlag) : base("EditBucket")
		{
			Content.UserId = userId;
			Content.Size = bucketSize;
			Content.Name = name;
			Content.BucketId = bucketId;
			Content.Public = publicFlag;
		}
	}
}