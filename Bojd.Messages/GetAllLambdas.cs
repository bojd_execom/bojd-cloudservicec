﻿using Bojd.Communication.Messages;
using Bojd.Messages.MessageContent;
using NetMQ;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bojd.Messages
{
	public class GetAllLambdas : MessageGet<UserMessage>
	{
		public GetAllLambdas(NetMQMessage message) : base(message)
		{
		}

		public GetAllLambdas(int userId) : base("AllLambdas")
		{
			Content.Id = userId;
		}
	}
}
