﻿using Bojd.Communication.Messages;
using Bojd.Messages.MessageContent;
using Google.Protobuf;
using NetMQ;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bojd.Messages
{
    public class EditLambda : MessagePublish<LambdaMessage>
    {
		public EditLambda(NetMQMessage message) : base(message)
		{
		}

		public EditLambda(byte[] fileContent, int id, int user_id) : base("EditLambda")
		{
			var byteString = ByteString.CopyFrom(fileContent);
			Content.FileContent = byteString;
			Content.UserId = user_id;
			Content.Id = id;
		}
    }
}
