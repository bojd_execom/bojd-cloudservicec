﻿using Bojd.Communication.Messages;
using Bojd.Messages.MessageContent;
using NetMQ;

namespace Bojd.Messages
{
    public class ErrorLogOnUser : MessagePublish<EmptyMessage>
    {
        public ErrorLogOnUser(NetMQMessage message) : base(message)
        {
        }

        public ErrorLogOnUser(NetMQFrame idFrame) : base("ErrorLogOnUser")
        {
            Id = idFrame;
        }

        public ErrorLogOnUser() : base("ErrorLogOnUser")
        {
        }
    }
}