﻿using Bojd.Communication.Messages;
using Bojd.Messages.MessageContent;
using NetMQ;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bojd.Messages
{
    public class GetSingleLambda : MessageGet<LambdaMessage>
    {
		public GetSingleLambda(NetMQMessage message) : base(message)
		{
		}

		public GetSingleLambda(int lambdaId) : base("SingleLambda")
		{
			Content.Id = lambdaId;
		}

    }
}
