﻿using Bojd.Communication.Messages;
using Bojd.Messages.MessageContent;
using NetMQ;

namespace Bojd.Messages
{
    public class FileList : MessagePublish<FilesInfoMessage>
    {
        public FileList(NetMQMessage message) : base(message)
        {
        }
    }
}