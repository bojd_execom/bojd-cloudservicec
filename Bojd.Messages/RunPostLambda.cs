﻿using Bojd.Communication.Messages;
using Bojd.Messages.MessageContent;
using NetMQ;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bojd.Messages
{
    public class RunPostLambda : MessagePublish<RunPostLambdaMessage>
    {
		public RunPostLambda(NetMQMessage message) : base(message)
		{
		}

		public RunPostLambda(string jsonContent, string guid) : base("RunPostLambda")
		{
			Content.JsonContent = jsonContent;
			Content.Guid = guid;
		}
    }
}
