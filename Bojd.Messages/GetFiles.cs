﻿using Bojd.Communication.Messages;
using Bojd.Messages.MessageContent;
using NetMQ;

namespace Bojd.Messages
{
    public class GetFiles : MessageGet<BucketMessage>
    {
        public GetFiles(NetMQMessage message) : base(message)
        {
        }

        public GetFiles(int bucketId) : base("Files")
        {
            Content.BucketId = bucketId;
        }
    }
}