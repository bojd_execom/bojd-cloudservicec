﻿using Bojd.Communication.Messages;
using Bojd.Messages.MessageContent;
using Google.Protobuf;
using NetMQ;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bojd.Messages
{
    public class UploadLambda : MessagePublish<LambdaMessage>
    {
		public UploadLambda(NetMQMessage message) : base(message)
		{
		}

		public UploadLambda(byte[] lambdaInBytes, string lambdaName, string technology, string type, string trigger, int userId) : base("UploadLambda")
		{
			var byteString = ByteString.CopyFrom(lambdaInBytes);
			Content.FileContent = byteString;
			Content.Name = lambdaName;
			Content.Technology = technology;
			Content.Type = type;
			Content.Trigger = trigger;
			Content.UserId = userId;
		}
    }
}
