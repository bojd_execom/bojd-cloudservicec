﻿using Bojd.Communication.Messages;
using Bojd.Messages.MessageContent;
using NetMQ;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bojd.Messages
{
    public class RunLambda : MessagePublish<LambdaMessage>
    {
		public RunLambda(string guid) : base("RunLambda")
		{
			Content.Guid = guid;
		}

    }
}
