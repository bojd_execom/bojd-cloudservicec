﻿using Bojd.Communication.Messages;
using Bojd.Messages.MessageContent;
using Google.Protobuf;
using NetMQ;

namespace Bojd.Messages
{
    public class UploadFile : MessagePublish<FileMessage>
    {
        public UploadFile(NetMQMessage message) : base(message)
        {
        }

        public UploadFile(byte[] fileInBytes, int bucketId, string fileName, string extension, int size = 0) : base("UploadFile")
        {
            var byteString = ByteString.CopyFrom(fileInBytes);
            Content.FileContent = byteString;
			Content.BucketId = bucketId;
			Content.FileName = fileName;
			Content.Extension = extension;
			Content.Size = size;
        }

	}
}