﻿using Bojd.Communication.Messages;
using Bojd.Messages.MessageContent;
using NetMQ;

namespace Bojd.Messages
{
    public class ErrorCreateUser : MessagePublish<ErrorMessage>
    {
        public ErrorCreateUser(NetMQMessage message) : base(message)
        {
        }

        public ErrorCreateUser(NetMQFrame idFrame) : base("ErrorCreateUser")
        {
            Id = idFrame;
        }

        public ErrorCreateUser() : base("ErrorCreateUser")
        {
        }
    }
}