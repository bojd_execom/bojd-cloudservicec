﻿using Bojd.Communication.Messages;
using Bojd.Messages.MessageContent;
using NetMQ;

namespace Bojd.Messages
{
    public class UserLoggedOn : MessagePublish<TokenMessage>
    {
        public UserLoggedOn(NetMQMessage message) : base(message)
        {
        }

        public UserLoggedOn(string value, int expiresIn) : base("UserLoggedOn")
        {
            Content.Value = value;
            Content.ExpiresIn = expiresIn;
        }

        public UserLoggedOn(string value, int expiresIn, NetMQFrame idFrame) : base("UserLoggedOn")
        {
            Content.Value = value;
            Content.ExpiresIn = expiresIn;
            Id = idFrame;
        }
    }
}