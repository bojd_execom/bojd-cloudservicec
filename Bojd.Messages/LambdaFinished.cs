﻿using Bojd.Communication.Messages;
using Bojd.Messages.MessageContent;
using NetMQ;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bojd.Messages
{
    public class LambdaFinished : MessagePublish<LambdaFinishedMessage>
    {
		public LambdaFinished(NetMQMessage message) : base(message)
		{
		}

		public LambdaFinished(NetMQFrame idFrame, string text) : base("LambdaFinished")
		{
			this.Id = idFrame;
			this.Content.Message = text;
		}
	}
}
