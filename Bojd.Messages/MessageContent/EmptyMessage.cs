﻿using Google.Protobuf;
using Google.Protobuf.Reflection;

namespace Bojd.Messages.MessageContent
{
    public sealed class EmptyMessage : IMessage<EmptyMessage>
    {
        public MessageDescriptor Descriptor => throw new System.NotImplementedException();

        public int CalculateSize()
        {
            throw new System.NotImplementedException();
        }

        public EmptyMessage Clone()
        {
            throw new System.NotImplementedException();
        }

        public bool Equals(EmptyMessage other)
        {
            throw new System.NotImplementedException();
        }

        public void MergeFrom(EmptyMessage message)
        {
            throw new System.NotImplementedException();
        }

        public void MergeFrom(CodedInputStream input)
        {
            throw new System.NotImplementedException();
        }

        public void WriteTo(CodedOutputStream output)
        {
        }
    }
}