// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: RunPostLambdaMessage.proto
// </auto-generated>
#pragma warning disable 1591, 0612, 3021
#region Designer generated code

using pb = global::Google.Protobuf;
using pbc = global::Google.Protobuf.Collections;
using pbr = global::Google.Protobuf.Reflection;
using scg = global::System.Collections.Generic;
namespace Bojd.Messages.MessageContent {

  /// <summary>Holder for reflection information generated from RunPostLambdaMessage.proto</summary>
  public static partial class RunPostLambdaMessageReflection {

    #region Descriptor
    /// <summary>File descriptor for RunPostLambdaMessage.proto</summary>
    public static pbr::FileDescriptor Descriptor {
      get { return descriptor; }
    }
    private static pbr::FileDescriptor descriptor;

    static RunPostLambdaMessageReflection() {
      byte[] descriptorData = global::System.Convert.FromBase64String(
          string.Concat(
            "ChpSdW5Qb3N0TGFtYmRhTWVzc2FnZS5wcm90bxIcQm9qZC5NZXNzYWdlcy5N",
            "ZXNzYWdlQ29udGVudCI5ChRSdW5Qb3N0TGFtYmRhTWVzc2FnZRITCgtKc29u",
            "Q29udGVudBgBIAEoCRIMCgRHdWlkGAIgASgJYgZwcm90bzM="));
      descriptor = pbr::FileDescriptor.FromGeneratedCode(descriptorData,
          new pbr::FileDescriptor[] { },
          new pbr::GeneratedClrTypeInfo(null, new pbr::GeneratedClrTypeInfo[] {
            new pbr::GeneratedClrTypeInfo(typeof(global::Bojd.Messages.MessageContent.RunPostLambdaMessage), global::Bojd.Messages.MessageContent.RunPostLambdaMessage.Parser, new[]{ "JsonContent", "Guid" }, null, null, null)
          }));
    }
    #endregion

  }
  #region Messages
  public sealed partial class RunPostLambdaMessage : pb::IMessage<RunPostLambdaMessage> {
    private static readonly pb::MessageParser<RunPostLambdaMessage> _parser = new pb::MessageParser<RunPostLambdaMessage>(() => new RunPostLambdaMessage());
    private pb::UnknownFieldSet _unknownFields;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pb::MessageParser<RunPostLambdaMessage> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::Bojd.Messages.MessageContent.RunPostLambdaMessageReflection.Descriptor.MessageTypes[0]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public RunPostLambdaMessage() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public RunPostLambdaMessage(RunPostLambdaMessage other) : this() {
      jsonContent_ = other.jsonContent_;
      guid_ = other.guid_;
      _unknownFields = pb::UnknownFieldSet.Clone(other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public RunPostLambdaMessage Clone() {
      return new RunPostLambdaMessage(this);
    }

    /// <summary>Field number for the "JsonContent" field.</summary>
    public const int JsonContentFieldNumber = 1;
    private string jsonContent_ = "";
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public string JsonContent {
      get { return jsonContent_; }
      set {
        jsonContent_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }

    /// <summary>Field number for the "Guid" field.</summary>
    public const int GuidFieldNumber = 2;
    private string guid_ = "";
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public string Guid {
      get { return guid_; }
      set {
        guid_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override bool Equals(object other) {
      return Equals(other as RunPostLambdaMessage);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public bool Equals(RunPostLambdaMessage other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (JsonContent != other.JsonContent) return false;
      if (Guid != other.Guid) return false;
      return Equals(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override int GetHashCode() {
      int hash = 1;
      if (JsonContent.Length != 0) hash ^= JsonContent.GetHashCode();
      if (Guid.Length != 0) hash ^= Guid.GetHashCode();
      if (_unknownFields != null) {
        hash ^= _unknownFields.GetHashCode();
      }
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void WriteTo(pb::CodedOutputStream output) {
      if (JsonContent.Length != 0) {
        output.WriteRawTag(10);
        output.WriteString(JsonContent);
      }
      if (Guid.Length != 0) {
        output.WriteRawTag(18);
        output.WriteString(Guid);
      }
      if (_unknownFields != null) {
        _unknownFields.WriteTo(output);
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int CalculateSize() {
      int size = 0;
      if (JsonContent.Length != 0) {
        size += 1 + pb::CodedOutputStream.ComputeStringSize(JsonContent);
      }
      if (Guid.Length != 0) {
        size += 1 + pb::CodedOutputStream.ComputeStringSize(Guid);
      }
      if (_unknownFields != null) {
        size += _unknownFields.CalculateSize();
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(RunPostLambdaMessage other) {
      if (other == null) {
        return;
      }
      if (other.JsonContent.Length != 0) {
        JsonContent = other.JsonContent;
      }
      if (other.Guid.Length != 0) {
        Guid = other.Guid;
      }
      _unknownFields = pb::UnknownFieldSet.MergeFrom(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(pb::CodedInputStream input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, input);
            break;
          case 10: {
            JsonContent = input.ReadString();
            break;
          }
          case 18: {
            Guid = input.ReadString();
            break;
          }
        }
      }
    }

  }

  #endregion

}

#endregion Designer generated code
