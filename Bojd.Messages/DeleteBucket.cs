﻿using Bojd.Communication.Messages;
using Bojd.Messages.MessageContent;

namespace Bojd.Messages
{
	public class DeleteBucket : MessagePublish<BucketMessage>
	{
		public DeleteBucket(int userId, int bucketId) : base("DeleteBucket")
		{
			Content.UserId = userId;
			Content.BucketId = bucketId;
		}
	}
}