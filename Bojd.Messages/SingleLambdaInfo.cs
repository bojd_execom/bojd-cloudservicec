﻿using Bojd.Communication.Messages;
using Bojd.Messages.MessageContent;
using NetMQ;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bojd.Messages
{
    public class SingleLambdaInfo : MessagePublish<LambdaMessage>
    {
		public SingleLambdaInfo(NetMQMessage message) : base(message)
		{
		}

    }
}
