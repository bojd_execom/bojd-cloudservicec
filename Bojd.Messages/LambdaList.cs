﻿using Bojd.Communication.Messages;
using Bojd.Messages.MessageContent;
using NetMQ;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bojd.Messages
{
    public class LambdaList : MessagePublish<LambdasInfoMessage>
    {
		public LambdaList(NetMQMessage message) : base(message)
		{
		}

    }
}
