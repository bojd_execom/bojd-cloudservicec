﻿using Bojd.Communication.Messages;
using Bojd.Messages.MessageContent;
using NetMQ;

namespace Bojd.Messages
{
    public class LogOnUser : MessagePublish<UserMessage>
    {
        public LogOnUser(NetMQMessage message) : base(message)
        {
        }

        public LogOnUser(int id, string userName, string password) : base("LogOnUser")
        {
            Content.Id = id;
            Content.UserName = userName;
            Content.Password = password;
        }

        public LogOnUser(int id, string userName, string password, NetMQFrame idFrame) : base("LogOnUser")
        {
            Content.Id = id;
            Content.UserName = userName;
            Content.Password = password;
            Id = idFrame;
        }
    }
}