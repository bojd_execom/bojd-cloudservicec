﻿using NetMQ;
using NetMQ.Sockets;

namespace Bojd.Communication.Broker
{
    public abstract class BrokerBase
    {
        private DealerSocket _dealer;
        private NetMQPoller _poller;
        private PublisherSocket _publisher;

        protected BrokerBase()
        {
            _dealer = new DealerSocket();
            _publisher = new PublisherSocket();
            _poller = new NetMQPoller { _dealer };
        }

        public void Publish(NetMQMessage message)
        {
            _publisher.SendMultipartMessage(message);
        }

        public void Start()
        {
            _dealer.ReceiveReady += HandleMessageOnDealer;

            _dealer.Bind(Configuration.BrokerBindingDealerAddress);
            _publisher.Bind(Configuration.BrokerBindingPublisherAddress);

            _poller.Run();
        }

        public void Stop()
        {
            _poller.Stop();
            _dealer.Close();
            _publisher.Close();
        }

        private void HandleMessageOnDealer(object sender, NetMQSocketEventArgs e)
        {
            NetMQMessage message = null;
            while (e.Socket.TryReceiveMultipartMessage(ref message))
            {
                Publish(message);
            }
        }
    }
}