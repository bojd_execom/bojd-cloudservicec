﻿using Bojd.Communication.Messages;
using NetMQ;
using NetMQ.Sockets;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bojd.Communication.Services
{
    public abstract class ServiceBase
    {
        private DealerSocket _dealer;
        private NetMQPoller _poller;
        private SubscriberSocket _subscriber;
        private Dictionary<string, Action<NetMQMessage>> _subscriptions = new Dictionary<string, Action<NetMQMessage>>();

        protected ServiceBase()
        {
            _dealer = new DealerSocket();
            _subscriber = new SubscriberSocket();
            _poller = new NetMQPoller { _subscriber };
        }

        public void Send(NetMQMessage netMQMessage)
        {
            _dealer.SendMultipartMessage(netMQMessage);
        }

        public void Start()
        {
            _subscriber.ReceiveReady += HandleMessageOnSubscriber;

            _dealer.Connect(Configuration.BrokerDealerAddress);
            _subscriber.Connect(Configuration.BrokerPublisherAddress);

            _poller.Run();
        }

        public void Stop()
        {
            _poller.Stop();
            _dealer.Close();
            _subscriber.Close();
        }

        public void Subscribe(string topic, Action<NetMQMessage> handler)
        {
            if (_subscriptions.ContainsKey(topic))
            {
                return;
            }

            _subscriptions.Add(topic, handler);

            _subscriber.Subscribe(topic);
        }


        public void Unsubscribe(string topic)
        {
            _subscriber.Unsubscribe(topic);
        }

        private void HandleMessageOnSubscriber(object sender, NetMQSocketEventArgs e)
        {
            NetMQMessage message = null;
            while (e.Socket.TryReceiveMultipartMessage(ref message))
            {
                string topic = message.ReadTopic();

                _subscriptions.FirstOrDefault(subscription => subscription.Key == topic).Value?.Invoke(message);
            }
        }
    }
}