﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bojd.Communication
{
    public static class Configuration
    {
        public const string BrokerBindingDealerAddress = "tcp://*:9000";

        public const string BrokerBindingPublisherAddress = "tcp://*:9001";

        public const string BrokerDealerAddress = "tcp://localhost:9000";

        public const string BrokerPublisherAddress = "tcp://localhost:9001";
    }
}
