﻿using Google.Protobuf;
using NetMQ;

namespace Bojd.Communication.Messages
{
    public abstract class MessageGet<T> : MessageBase<T> where T : IMessage, new()
    {
        protected MessageGet(string obj) : base(obj)
        {
            Predicate = "Get";
        }

        protected MessageGet(NetMQMessage message) : base(message)
        {
        }
    }
}
