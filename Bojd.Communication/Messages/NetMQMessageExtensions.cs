﻿using NetMQ;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bojd.Communication.Messages
{
    internal static class NetMQMessageExtensions
    {
        public static string ReadTopic(this NetMQMessage message)
        {
            return message.First.ConvertToString(Encoding.UTF8);
        }
    }
}
