﻿using Google.Protobuf;
using NetMQ;

namespace Bojd.Communication.Messages
{
    public class MessageCreate<T> : MessageBase<T> where T : IMessage, new()
    {
        protected MessageCreate(string obj) : base(obj)
        {
            Predicate = "Create";
        }

        protected MessageCreate(NetMQMessage message) : base(message)
        {
        }
    }
}
