﻿using Google.Protobuf;
using NetMQ;
using System.IO;
using System.Text;

namespace Bojd.Communication.Messages
{
    public abstract class MessageBase<T> where T : IMessage, new()
    {
        protected MessageBase(string obj)
        {
            Object = obj;
        }

        protected MessageBase(NetMQMessage message)
        {
            string[] topic = message.Pop().ConvertToString(Encoding.UTF8).Split(".");

            if (topic.Length == 2)
            {
                Predicate = topic[0];
                Object = topic[1];
            }

            Id = message.Pop();

            byte[] content = message.Pop().ToByteArray();
            Content.MergeFrom(content);
        }

        public T Content { get; } = new T();

        public NetMQFrame Id { get; set; }

        public bool IsValidMessage {
            get {
                return (Predicate == "Get" || Predicate == "Publish") && !string.IsNullOrEmpty(Object);
            }
        }

        public string Object { get; protected set; }

        public string Predicate { get; protected set; }

        public string Topic {
            get {
                return $"{Predicate}.{Object}";
            }
        }

        public NetMQMessage ToNetMQMessage()
        {
            NetMQMessage message = new NetMQMessage();

            message.Append(Topic, Encoding.UTF8);

            if (Id != null)
            {
                message.Append(Id);
            }
            else
            {
                message.AppendEmptyFrame();
            }

            var stream = new MemoryStream();
            Content.WriteTo(stream);

            message.Append(new NetMQFrame(stream.ToArray()));

            return message;
        }
    }
}