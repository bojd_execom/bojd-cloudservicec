﻿using Google.Protobuf;
using NetMQ;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bojd.Communication.Messages
{
    public abstract class MessagePublish<T> : MessageBase<T> where T : IMessage, new()
    {
        protected MessagePublish(string obj) : base(obj)
        {
            Predicate = "Publish";
        }

        protected MessagePublish(NetMQMessage message) : base(message)
        {
        }
    }
}
