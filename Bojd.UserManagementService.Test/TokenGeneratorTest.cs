using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using Bojd.Common;
using Bojd.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bojd.API.Test
{
    [TestClass]
    public class TokenGeneratorTest
    {
        [TestMethod]
        public void GenerateTokenAndCheckIfItIsProperllySerialized()
        {
            BojdToken token = TokenGenerator.GenerateToken("1", "ogi", "testkeytestkeytestkeytestkeytestkey", DateTime.UtcNow.AddMinutes(50), "test", "test");
            var handler = new JwtSecurityTokenHandler();
            var parsedToken = handler.ReadToken(token.Value) as JwtSecurityToken;
            Assert.AreEqual("ogi", parsedToken.Claims.First(claim => claim.Type == ClaimTypes.Name).Value);
            Assert.IsTrue(Math.Abs((DateTime.UtcNow.AddMinutes(50) - parsedToken.ValidTo).TotalSeconds) < 1);
            Assert.AreEqual("test", parsedToken.Issuer);
            Assert.AreEqual("test", parsedToken.Audiences.FirstOrDefault());
        }

        [TestMethod]
        public void GenerateTokenForSameUserTwiceCreateDifferentTokens()
        {
            BojdToken firstToken = TokenGenerator.GenerateToken("1", "ogi", "testkeytestkeytestkeytestkeytestkey", DateTime.Now.AddMinutes(50), "test", "test");
            BojdToken secondToken = TokenGenerator.GenerateToken("1", "ogi", "testkeytestkeytestkeytestkeytestkey", DateTime.Now.AddMinutes(50), "test", "test");
            Assert.AreNotEqual(firstToken.Value, secondToken.Value);
        }
    }
}