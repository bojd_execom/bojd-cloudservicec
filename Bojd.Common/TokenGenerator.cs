﻿using Bojd.Models;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Bojd.Common
{
    public static class TokenGenerator
    {
        public static BojdToken GenerateToken(string claimName, string claimId, string securityKey, DateTime expireDate, string issuer, string audience)
        {
            if (claimName != null && securityKey != null && expireDate != null && issuer != null && audience != null)
            {
                var claimsdata = new[] { new Claim(ClaimTypes.Name, claimName),
                                         new Claim(ClaimTypes.Sid, claimId) };
                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(securityKey));
                var signInCard = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature);

                var _token = new JwtSecurityToken(
                    issuer: issuer,
                    audience: audience,
                    expires: expireDate,
                    claims: claimsdata,
                    signingCredentials: signInCard
                );

                var tokenString = new JwtSecurityTokenHandler().WriteToken(_token);

                BojdToken bToken = new BojdToken() { ExpiresIn = (long)expireDate.Subtract(DateTime.Now).TotalMinutes, Value = tokenString };

                return bToken;
            }

            return null;
        }
    }
}