﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Bojd.Common
{
    public interface IHttpClient
    {
        HttpResponseMessage Get(string url);

        Task<HttpResponseMessage> GetAsync(string url);

        HttpResponseMessage Post(string url, HttpContent content);

        Task<HttpResponseMessage> PostAsync(string url, HttpContent content);
    }
}