﻿using NetMQ;

namespace Bojd.Common
{
    public static class MessageUtils
    {
        public static NetMQMessage ConvertDealerMessageToRouterMessage(NetMQMessage message)
        {
            NetMQMessage alignedMessage = new NetMQMessage(4);
            alignedMessage.Append(message[1]); // id
            alignedMessage.AppendEmptyFrame(); // empty
            alignedMessage.Append(message[0]); // topic
            alignedMessage.AppendEmptyFrame(); // empty id slot
            alignedMessage.Append(message[2]); // content
            return alignedMessage;
        }

        public static NetMQMessage ConvertRouterMessageToDealerMessage(NetMQMessage message)
        {
            NetMQMessage alignedMessage = new NetMQMessage(3);
            alignedMessage.Append(message[2]);  // topic
            alignedMessage.Append(message[0]);  // id
            alignedMessage.Append(message[4]);  // content
            return alignedMessage;
        }
    }
}