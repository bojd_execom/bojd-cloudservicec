﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Bojd.Common
{
    public class HttpClientHandler : IHttpClient
    {
        private HttpClient httpClient = new HttpClient();

        public HttpResponseMessage Get(string url)
        {
            throw new NotImplementedException();
        }

        public Task<HttpResponseMessage> GetAsync(string url)
        {
            return httpClient.GetAsync(url);
        }

        public HttpResponseMessage Post(string url, HttpContent content)
        {
            throw new NotImplementedException();
        }

        public Task<HttpResponseMessage> PostAsync(string url, HttpContent content)
        {
            return httpClient.PostAsync(url, content);
        }
    }
}