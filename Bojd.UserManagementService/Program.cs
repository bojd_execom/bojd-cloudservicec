﻿using Bojd.UserManagementService.Communication;
using Bojd.UserManagementService.Repositories;
using Bojd.UserManagementService.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;

namespace Bojd.UserManagementService
{
    // TODO: extract configuration in class and use sharedsettings.json
    internal class Program
    {
        private static IConfiguration _configuration;

        private static DataContext ConfigurationStart()
        {
            var builder = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
               .AddJsonFile(Path.GetFullPath(Path.Combine(AppContext.BaseDirectory, "..\\..\\..\\..\\Bojd.Common\\sharedsettings.json")));

            IConfigurationRoot configuration = builder.Build();
            _configuration = configuration;

            var serviceProvider = new ServiceCollection()
                .AddLogging()
                .AddSingleton<IConfiguration>(configuration)
                .BuildServiceProvider();

            DataContext.Configuration = configuration;

            DbContextOptionsBuilder<DataContext> optionsBuilder = new DbContextOptionsBuilder<DataContext>()
                .UseSqlServer(configuration.GetConnectionString("Default"));

            return new DataContext(optionsBuilder.Options);
        }

        private static void Main(string[] args)
        {
            using (DataContext dc = ConfigurationStart())
            {
                dc.Database.EnsureCreated();
            }

            UserService _userService = new UserService(_configuration);

            Console.WriteLine("Starting user management service...");
            UserCommunicationService userCommunicationService = new UserCommunicationService(_userService);
            userCommunicationService.Start();
        }
    }
}