﻿using Bojd.Common;
using Bojd.Models;
using Bojd.UserManagementService.Entities;
using Bojd.UserManagementService.Repositories;
using Bojd.UserManagementService.Validation;
using Microsoft.Extensions.Configuration;
using System;

namespace Bojd.UserManagementService.Services
{
    public class UserService
    {
        private IConfiguration _configuration;
        private IRepository<UserEntity> _userRepository = new Repository<UserEntity>();

        public UserService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public Either<ServiceError, bool> Add(UserEntity user)
        {
            using (DataContext context = new DataContext())
            {
                UserValidator userValidator = new UserValidator(user);
                if (!userValidator.Test())
                {
                    return new ServiceError(400, userValidator.Results);
                }
                var usersFromDb = _userRepository.Find((u => u.UserName == user.UserName), context);
                foreach (var userFromDb in usersFromDb)
                {
                    if ((user.UserName == userFromDb.UserName))   // additional case sensitive check (database can't do case sensitive checks)
                    {
                        return new ServiceError(409, "User with given username already exists.");
                    }
                }
                _userRepository.Add(user, context);
                return true;
            }
        }

        public Either<ServiceError, BojdToken> LogOn(UserEntity user)
        {
            using (DataContext context = new DataContext())
            {
                var usersFromDb = _userRepository.Find((u => u.UserName == user.UserName && (u.Password == user.Password)), context);
                foreach (var userFromDb in usersFromDb)
                {
                    if ((user.UserName == userFromDb.UserName) && (userFromDb.Password == user.Password))   // additional case sensitive check (database can't do case sensitive checks)
                    {
                        return TokenGenerator.GenerateToken(
                            userFromDb.UserName,
                            userFromDb.Id.ToString(),
                            _configuration["Token:Key"],
                            DateTime.Now.AddMinutes(Double.Parse(_configuration["Token:ValidForMinutes"])),
                            _configuration["Token:Issuer"],
                            _configuration["Token:Audience"]);
                    }
                }
                return new ServiceError(401, "Wrong username or password.");
            }
        }
    }
}