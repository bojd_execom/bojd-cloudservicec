﻿using Bojd.UserManagementService.Entities;
using System.Linq;

namespace Bojd.UserManagementService.Validation
{
    public class UserValidator : Validator
    {
        private const int PASSWORD_MAX_LENGTH = 30;
        private const int PASSWORD_MIN_LENGTH = 8;
        private const int USERNAME_MAX_LENGTH = 30;
        private const int USERNAME_MIN_LENGTH = 4;
        private UserEntity user;

        public UserValidator(UserEntity user)
        {
            this.user = user;
        }

        public override bool Test()
        {
            bool isValid = true;
            string userName = user.UserName.Trim();
            string userPassword = user.Password.Trim();
            if (string.IsNullOrEmpty(userName)
                || string.IsNullOrEmpty(userPassword))
            {
                AddResult("Username and password must not be empty!");
                isValid = false;
            }
            else if (userName.Length < USERNAME_MIN_LENGTH || userName.Length > USERNAME_MAX_LENGTH)
            {
                AddResult("Username must be between " + USERNAME_MIN_LENGTH + " and " + USERNAME_MAX_LENGTH + " characters long.");
                isValid = false;
            }
            else if (userPassword.Length < PASSWORD_MIN_LENGTH || userPassword.Length > PASSWORD_MAX_LENGTH)
            {
                AddResult("Password must be between " + PASSWORD_MIN_LENGTH + " and " + PASSWORD_MAX_LENGTH + " characters long.");
                isValid = false;
            }
            else if (userName.Any(char.IsWhiteSpace))
            {
                AddResult("Username cannot contain spaces.");
                isValid = false;
            }
            else if (!userName.All(char.IsLetterOrDigit) || !userPassword.All(char.IsLetterOrDigit))
            {
                AddResult("Username and password must be alphanumeric.");
                isValid = false;
            }
            return isValid;
        }
    }
}