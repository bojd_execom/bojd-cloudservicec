﻿using System;

namespace Bojd.UserManagementService.Validation
{
    public abstract class Validator
    {
        public string Results { get; set; }

        public abstract bool Test();

        protected void AddResult(string result)
        {
            Results += result + Environment.NewLine;
        }
    }
}