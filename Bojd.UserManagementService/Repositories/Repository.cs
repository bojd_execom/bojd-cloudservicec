﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Bojd.UserManagementService.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        public void Add(T entity, DataContext context)
        {
            context.Set<T>().Add(entity);
            context.SaveChanges();
        }

        public void Delete(T entity, DataContext context)
        {
            throw new NotImplementedException();
        }

        public void Edit(T entity, DataContext context)
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> Find(Expression<Func<T, bool>> predicate, DataContext context)
        {
            return context.Set<T>().Where(predicate);
        }

        public IQueryable<T> GetAll(DataContext context)
        {
            throw new NotImplementedException();
        }
    }
}