﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Bojd.UserManagementService.Repositories
{
    public interface IRepository<T> where T : class
    {
        void Add(T entity, DataContext context);

        void Delete(T entity, DataContext context);

        void Edit(T entity, DataContext context);

        IQueryable<T> Find(Expression<Func<T, bool>> predicate, DataContext context);

        IQueryable<T> GetAll(DataContext context);
    }
}