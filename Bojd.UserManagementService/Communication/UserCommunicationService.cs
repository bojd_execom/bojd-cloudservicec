﻿using Bojd.Communication.Services;
using Bojd.Messages;
using Bojd.UserManagementService.Services;
using NetMQ;

namespace Bojd.UserManagementService.Communication
{
    public class UserCommunicationService : ServiceBase
    {
        private UserService _userService;

        public UserCommunicationService(UserService userService) : base()
        {
            _userService = userService;
            Subscribe("Publish.CreateUser", HandleMessageCreateUser);
            Subscribe("Publish.LogOnUser", HandleLogOnUser);
        }

        public void HandleMessageCreateUser(NetMQMessage obj)
        {
            var idFrame = obj[1];

            CreateUser createUser = new CreateUser(obj);

            if (!createUser.IsValidMessage)
            {
                SendErrorMessage(idFrame, 400, "Bad request, message is not valid");
                return;
            }

            var result = _userService.Add(new Entities.UserEntity { UserName = createUser.Content.UserName, Password = createUser.Content.Password });
            result.Match(
                    error =>
                    {
                        SendErrorMessage(idFrame, error.Code, error.Text);
                        return false;
                    },
                    success =>
                    {
                        UserCreated userCreated = new UserCreated(idFrame);
                        Send(userCreated.ToNetMQMessage());
                        return true;
                    }
                );
        }

        private void HandleLogOnUser(NetMQMessage obj)
        {
            // TODO: id separation will be needed in every method, consider extracting in some way
            var idFrame = obj[1];

            LogOnUser logOnUser = new LogOnUser(obj);

            var result = _userService.LogOn(new Entities.UserEntity { UserName = logOnUser.Content.UserName, Password = logOnUser.Content.Password });

            result.Match(
                    error =>
                    {
                        SendErrorMessage(idFrame, error.Code, error.Text);
                        return false;
                    },
                    token =>
                    {
                        UserLoggedOn userLoggedOn = new UserLoggedOn(token?.Value, (int)token?.ExpiresIn, idFrame);
                        Send(userLoggedOn.ToNetMQMessage());
                        return true;
                    }
                );
        }

        private void SendErrorMessage(NetMQFrame idFrame, int code, string text)
        {
            Error error = new Error(idFrame, code, text);
            Send(error.ToNetMQMessage());
        }
    }
}