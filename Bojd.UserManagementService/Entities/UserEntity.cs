﻿using Bojd.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bojd.UserManagementService.Entities
{
    public class UserEntity
    {
        public UserEntity()
        {
        }

        public UserEntity(User user)
        {
            Id = user.Id;
            Password = user.Password;
            UserName = user.UserName;
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Password { get; set; }
        public string UserName { get; set; }
    }
}